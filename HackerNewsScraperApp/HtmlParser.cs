﻿using System;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace HackerNewsScraperApp
{
    public class HtmlParser
    {
        // Get any generic HTML element based on the xpath and print a warning if its not found
        public static HtmlNode GetElement(HtmlNode documentNode, string xpath, string usage, string id)
        {
            var element = documentNode.SelectSingleNode(xpath);
            if (element != null) return element;
            PrintWarning($"No {usage} element found for {id}");
            return HtmlNode.CreateNode("<a></a>");
        }

        // Get the URI href for the given post based on the XPath and print a warning message if not found or invalid link
        public static string GetUri(HtmlNode documentNode, string xpath, string id)
        {
            var element = GetElement(documentNode, xpath, "URI",id);
            var attribute = element.GetAttributeValue("href", "");
            // Regex to check if the returned string is a valid URL
            var isValidUrlCheck = Regex.IsMatch(attribute,
                @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$");
            if (isValidUrlCheck) return attribute;
            PrintWarning($"No URI attribute not found for {id} or URI is invalid");
            return string.Empty;
        }

        // Get any of the number elements based on the Xpath and parse to an int
        public static int GetNumberElements(HtmlNode documentNode, string xpath, string usage, string id)
        {
            var commentsText = GetElement(documentNode, xpath, usage, id).InnerText;
            var numberPart = Regex.Match(commentsText, @"\d+").Value.Trim();
            int.TryParse(numberPart,out var points);
            return points;
        }

        // Print a warning in yellow to the Console
        private static void PrintWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
