﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace HackerNewsScraperApp
{
    public class Scraper
    {
        private static readonly HtmlWeb HtmlWeb = new HtmlWeb();
        private static readonly List<Article> Articles = new List<Article>();
        private static readonly List<string> ArticleIds = new List<string>();

        public static List<Article> ScrapeHackerNewsArticles(int numberOfArticlesToGet)
        {
            var pageCounter = 1;

            //Load first page
            var page = HtmlWeb.Load("https://news.ycombinator.com/news?p=" + pageCounter);
            
            // Get Number of posts on the first page
            ArticleIds.AddRange(page.DocumentNode.SelectNodes("//tr[@class='athing']").Select(x => x.Id));
            
            // Calculate the number of pages to search
            var pageCount = Math.Ceiling((decimal)numberOfArticlesToGet/ArticleIds.Count);

            // Retrieve all post ids on each page
            while (pageCounter <= pageCount)
            {
                page = HtmlWeb.Load("https://news.ycombinator.com/news?p=" + pageCounter);
                ArticleIds.AddRange(page.DocumentNode.SelectNodes("//tr[@class='athing']").Select(x => x.Id));
                pageCounter++;
            }

            // Take the first X number of posts add to an article object
            foreach (var articleId in ArticleIds.Take(numberOfArticlesToGet))
            {
                var storyItem = HtmlWeb.Load($"https://news.ycombinator.com/item?id={articleId}");
                var article = new Article
                {
                    Title = HtmlParser.GetElement(storyItem.DocumentNode, "//a[@class='storylink']", "Title", articleId)
                        .InnerText,
                    Uri = HtmlParser.GetUri(storyItem.DocumentNode, "//a[@class='storylink']", articleId),
                    Author = HtmlParser.GetElement(storyItem.DocumentNode, "//a[@class='hnuser']", "Author", articleId)
                        .InnerText,
                    Points = HtmlParser.GetNumberElements(storyItem.DocumentNode, "//span[@class='score']",
                        "Points", articleId),
                    Comments = HtmlParser.GetNumberElements(storyItem.DocumentNode,
                        $"//td/a[@href='item?id={articleId}' and contains(text(),'&nbsp;comments')]", "Comments", articleId),
                    Rank = Articles.Count + 1
                };
                Articles.Add(article);
            }
            return Articles;
        }
    }
}
