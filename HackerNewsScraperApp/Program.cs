﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace HackerNewsScraperApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Check arguments have been passed otherwise throw error
            if (args.Length <= 0)
                throw  new ArgumentException("No arguments passed!! Must pass a number between 1 and 100");
            
            // Parse argument to get the number of posts to retreive
            int.TryParse(args[0],out var numberOfArticlesToGet);
            if (numberOfArticlesToGet <= 0 || numberOfArticlesToGet > 100)
                throw  new ArgumentException("Arguments invalid must be between 1 and 100");
            
            //Run the scraper
            var articles = Scraper.ScrapeHackerNewsArticles(numberOfArticlesToGet);
            
            //Serialize into JSON
            var jsonFormattedArticles = articles.Select(JsonConvert.SerializeObject);
            
            //Print JSON strings to STDOUT
            foreach (var jsonFormattedArticle in jsonFormattedArticles)
            {
                Console.WriteLine(jsonFormattedArticle);
                Console.WriteLine();
            }
        }
    }
}
