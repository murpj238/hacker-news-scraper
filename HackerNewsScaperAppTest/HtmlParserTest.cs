using HackerNewsScraperApp;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HackerNewsScaperAppTest
{
    [TestClass]
    public class HtmlParserTest
    {
        [TestMethod]
        public void Test_GetElement_Happy()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"title\"><a href=\"https://jobs.lever.co/buildzoom\" class=\"storylink\" rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetElement(htmlToCheck, "//a[@class='storylink']", "Title", "20717238");

            //Assert
            Assert.IsFalse(string.IsNullOrEmpty(result.InnerText));
            Assert.AreEqual(result.InnerText, "BuildZoom (YC W13) is hiring. Help us make remodeling cheaper");
        }

        [TestMethod]
        public void Test_GetElement_InvalidHtml()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetElement(htmlToCheck, "//a[@class='storylink']", "Title", "20717238");

            //Assert
            Assert.IsTrue(string.IsNullOrEmpty(result.InnerText));
        }

        [TestMethod]
        public void Test_GetElement_NoElementExists()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"title\"><a href=\"https://jobs.lever.co/buildzoom\" rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetElement(htmlToCheck, "//a[@class='storylink']", "Title", "20717238");

            //Assert
            Assert.IsTrue(string.IsNullOrEmpty(result.InnerText));
        }
        
        [TestMethod]
        public void Test_GetUri_Happy()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"title\"><a href=\"https://jobs.lever.co/buildzoom\" class=\"storylink\" rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetUri(htmlToCheck, "//a[@class='storylink']", "20717238");

            //Assert
            Assert.IsFalse(string.IsNullOrEmpty(result));
            Assert.AreEqual(result, "https://jobs.lever.co/buildzoom");
        }

        [TestMethod]
        public void Test_GetUri_InvalidHtml()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetUri(htmlToCheck, "//a[@class='storylink']", "20717238");

            //Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
        }

        [TestMethod]
        public void Test_Uri_NoElementExists()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"title\"><a href=\"https://jobs.lever.co/buildzoom\" rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetUri(htmlToCheck, "//a[@class='storylink']", "20717238");

            //Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
        }

        [TestMethod]
        public void Test_Uri_NoAttributeExists()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"title\"><a class=\"storylink\" rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetUri(htmlToCheck, "//a[@class='storylink']", "20717238");

            //Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
        }

        [TestMethod]
        public void Test_GetUri_InvalidLink()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"title\"><a class=\"storylink\"  href=\"odsfsdfm\" rel=\"nofollow\">BuildZoom (YC W13) is hiring. Help us make remodeling cheaper</a><span class=\"sitebit comhead\"> (<a href=\"from?site=lever.co\"><span class=\"sitestr\">lever.co</span></a>)</span></td>");
            
            //Act
            var result = HtmlParser.GetUri(htmlToCheck, "//a[@class='storylink']", "20717238");

            //Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
        }

        [TestMethod]
        public void Test_GetNumberElements_Happy()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"subtext\">\n        <span class=\"score\" id=\"score_20693998\">122 points</span> by <a href=\"user?id=sohkamyung\" class=\"hnuser\">sohkamyung</a> <span class=\"age\"><a href=\"item?id=20693998\">1 day ago</a></span> <span id=\"unv_20693998\"></span> | <a href=\"hide?id=20693998&amp;goto=item%3Fid%3D20693998\">hide</a> | <a href=\"https://hn.algolia.com/?query=A%20Novel%20Concept%3A%20Silent%20Book%20Clubs%20Offer%20Introverts%20a%20Space%20to%20Socialize&amp;sort=byDate&amp;dateRange=all&amp;type=story&amp;storyText=false&amp;prefix&amp;page=0\" class=\"hnpast\">past</a> | <a href=\"https://www.google.com/search?q=A%20Novel%20Concept%3A%20Silent%20Book%20Clubs%20Offer%20Introverts%20a%20Space%20to%20Socialize\">web</a> | <a href=\"fave?id=20693998&amp;auth=e7c973485d6acde4b30162850edd966c8503e498\">favorite</a> | <a href=\"item?id=20693998\">86&nbsp;comments</a>              </td>");
            
            //Act
            var result = HtmlParser.GetNumberElements(htmlToCheck, "//span[@class='score']",
                "Points", "20693998");

            //Assert
            Assert.IsFalse(result == 0);
            Assert.AreEqual(result,122);
        }

        [TestMethod]
        public void Test_GetNumberElements_NoElement()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"subtext\">\n        <span>122 points</span> by <a href=\"user?id=sohkamyung\" class=\"hnuser\">sohkamyung</a> <span class=\"age\"><a href=\"item?id=20693998\">1 day ago</a></span> <span id=\"unv_20693998\"></span> | <a href=\"hide?id=20693998&amp;goto=item%3Fid%3D20693998\">hide</a> | <a href=\"https://hn.algolia.com/?query=A%20Novel%20Concept%3A%20Silent%20Book%20Clubs%20Offer%20Introverts%20a%20Space%20to%20Socialize&amp;sort=byDate&amp;dateRange=all&amp;type=story&amp;storyText=false&amp;prefix&amp;page=0\" class=\"hnpast\">past</a> | <a href=\"https://www.google.com/search?q=A%20Novel%20Concept%3A%20Silent%20Book%20Clubs%20Offer%20Introverts%20a%20Space%20to%20Socialize\">web</a> | <a href=\"fave?id=20693998&amp;auth=e7c973485d6acde4b30162850edd966c8503e498\">favorite</a> | <a href=\"item?id=20693998\">86&nbsp;comments</a>              </td>");
            
            //Act
            var result = HtmlParser.GetNumberElements(htmlToCheck, "//span[@class='score']",
                "Points", "20693998");

            //Assert
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void Test_GetNumberElements_NotANumber()
        {
            //Arrange
            var htmlToCheck = HtmlNode.CreateNode(
                "<td class=\"subtext\">\n        <span class=\"score\" id=\"score_20693998\">points</span> by <a href=\"user?id=sohkamyung\" class=\"hnuser\">sohkamyung</a> <span class=\"age\"><a href=\"item?id=20693998\">1 day ago</a></span> <span id=\"unv_20693998\"></span> | <a href=\"hide?id=20693998&amp;goto=item%3Fid%3D20693998\">hide</a> | <a href=\"https://hn.algolia.com/?query=A%20Novel%20Concept%3A%20Silent%20Book%20Clubs%20Offer%20Introverts%20a%20Space%20to%20Socialize&amp;sort=byDate&amp;dateRange=all&amp;type=story&amp;storyText=false&amp;prefix&amp;page=0\" class=\"hnpast\">past</a> | <a href=\"https://www.google.com/search?q=A%20Novel%20Concept%3A%20Silent%20Book%20Clubs%20Offer%20Introverts%20a%20Space%20to%20Socialize\">web</a> | <a href=\"fave?id=20693998&amp;auth=e7c973485d6acde4b30162850edd966c8503e498\">favorite</a> | <a href=\"item?id=20693998\">86&nbsp;comments</a>              </td>");
            
            //Act
            var result = HtmlParser.GetNumberElements(htmlToCheck, "//span[@class='score']",
                "Points", "20693998");

            //Assert
            Assert.IsTrue(result == 0);
        }
    }
}
