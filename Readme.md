# HackerNews .Net Core Scraper

This is a .Net Core Console Application to scrape a given number of posts from the HackerNews website.

The solution contains a .Net Core console application and a Dockerfile. There is also a unit test project.

A gitlab-ci.yml controls the automated build and test of the console application.

## How to run the application

To run the application you must first install Docker, instruction can be found [here](https://docs.docker.com/install/).

The container is built on linux containers so must be run on a linux based machine or else you can switch to Linux containers via Docker Desktop on Windows.

To run the image run the following commands:
```
docker pull registry.gitlab.com/murpj238/hacker-news-scraper:<LAST COMMIT ID>
docker run -i registry.gitlab.com/murpj238/hacker-news-scraper:<LAST COMMIT ID> <NUMBER OF POSTS TO RETURN>
```

## Libraries used in the Application

In this application I used two external libraries.

### Html Agility Pack

Html Agility Pack is a is an HTML parser written in C# to read/write DOM and supports plain XPATH or XSLT.

I chose this package as it allows for easy interaction with XPath and simple manipulation of Html based web pages and elements.

### Newtonsoft JSON

Newtonsoft JSON or JSON.Net is a high performance JSON framework for .Net.

I chose this packages as it contains out of the box functions to convert objects into JSON strings.